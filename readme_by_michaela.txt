Read me:

* What I have done
	- Values choosen for the nice day: Temperature, humidity, wind, rain

	- Temperature and humidity are chosen with a range bar :
    This could have been also made with two sliders or two text fields, but I thought its better to use a ranged seek bar, this looks better and might work better.
    The temperature (since it needs a min temperature and a max temperature ) needed two inputs and to solve that with one element the range seekbar was chosen.

  - rain and wind visuals are showing a type of blinking animation (changes the blinking duration if the values changed)
    Original idea was to add 3 different layers of rain and make each layer blink in a different sequence and show number of layers depending on the chosen amount of rain

  - LinearLayouts usage - easy way for a quick application

  - tablets, landscape, tested on multiple devices (3) (also a bit older ones)

  - a few unit tests

  - based the temperature on 0-100 fahrenheit (since this is supposed to be the humanly bareable range of temperature)

* Version 2 (things that I would have liked to do still - not required by the assignment)
  - add save preferences to phone so that the next time you open the application you have your settings already there
  - add nice background images to the nice day text
  - change animation to original idea
  - pull to refresh ?
  - reusable Radio Group / Range Seekbar
  - move api to its seperate class
  - being able to choose between celsius and fahrenheit (Api also supports that, and with the percentage calculation, adding fahrenheit would be easy, because its only adding a range of temperatures )
  - current choosing buttons for rain and wind would get an image for each setting
  - would write more unit tests
  - adjust the ranges for the rain and wind

* Normally would do
  - smaller commits (it was quickly progressing and some features depended on each other eg.location and permissions)
  - normally would put gitignore from the begining of the project.