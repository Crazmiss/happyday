package com.happyday.michi.happyday;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static int SETTING_REQUEST_CODE = 1;

    private static final int LOCATION_PERMISSION = 1;
    private static final int INTERNET_PERMISSION = 2;
    private LocationManager locationManager;
    private String provider;
    DaySettings daySettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        daySettings = new DaySettings();


//        Check if i have permission to get the location information
        boolean isPermissionInternetGranted = checkInternetPermission();
        boolean isPermissionGranted = checkLocationPermission();

        if (isPermissionGranted && isPermissionInternetGranted) {
            requestWeatherData();
        } else {
            TextView textViewNiceDay = findViewById(R.id.textview_main_nicedaystatus);
            textViewNiceDay.setText(R.string.noPermissionsGranted);
        }


    }

    private Location getLastKnownLocation() {
        Location bestLocation = null;

        if (locationManager != null && checkLocationPermission() && checkInternetPermission()) {
            List<String> providers = locationManager.getProviders(true);
            for (String provider : providers) {

                Location l = locationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                    this.provider = provider;
                }
            }
        }
        return bestLocation;
    }

    //Todo: move into different file
    private void requestWeatherData() {

        if (!checkLocationPermission() && !checkInternetPermission()) {
            return;
        }

        Location location = getLastKnownLocation();

        if (location == null) {
            return;
        }

        TextView textViewNiceDay = findViewById(R.id.textview_main_nicedaystatus);
        textViewNiceDay.setText(R.string.calculating);

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("api.openweathermap.org")
                .addPathSegment("data")
                .addPathSegment("2.5")
                .addPathSegment("weather")
                .addQueryParameter("lat", String.valueOf(location.getLatitude()))
                .addQueryParameter("lon", String.valueOf(location.getLongitude()))
                .addQueryParameter("units", "metric")
                .addQueryParameter("APPID", getResources().getString(R.string.currentWeatherApiId))
                .build();

        OkHttpClient client = new OkHttpClient.Builder().build();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final Response r = response;

                if (r.body() != null) {
                    String jsonData = r.body().string();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(jsonData);
                        JSONObject mainObject = jsonObject.getJSONObject("main");
                        Weather currentWeather = new Weather();

                        currentWeather.setTemp(mainObject.has("temp") ? mainObject.getLong("temp") : 0.f);
                        currentWeather.setPressure(mainObject.has("pressure") ? mainObject.getInt("pressure") : 0);
                        currentWeather.setHumidity(mainObject.has("humidity") ? mainObject.getInt("humidity") : 0);
                        currentWeather.setTempMin(mainObject.has("temp_min") ? mainObject.getLong("temp_min") : 0.f);
                        currentWeather.setTempMax(mainObject.has("temp_max") ? mainObject.getLong("temp_max") : 0.f);

                        JSONObject windObject = jsonObject.has("wind") ? jsonObject.getJSONObject("wind") : null;
                        if (windObject != null) {
                            currentWeather.setWindSpeed(windObject.has("speed") ? windObject.getInt("speed") : 0.f);
                        }

                        JSONObject rainObject = jsonObject.has("rain") ? jsonObject.getJSONObject("rain") : null;
                        if (rainObject != null) {
                            currentWeather.setWindSpeed(rainObject.has("3h") ? rainObject.getInt("3h") : 0);
                        }

                        final boolean isNiceDay = HappyDayJudge.isNowHappyDay(currentWeather, daySettings, getResources().getInteger(R.integer.celsiusMin), getResources().getInteger(R.integer.celsiusMax));

                        Handler mainHandler = new Handler(getBaseContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                TextView textViewNiceDay = findViewById(R.id.textview_main_nicedaystatus);
                                textViewNiceDay.setText(isNiceDay ? getString(R.string.todayIsANiceDay) : getString(R.string.noNiceDayToday));
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    public void Settings(View view) {

        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("daySettings", daySettings);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, SETTING_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == SETTING_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra("daySettings")) {
                    daySettings = data.getParcelableExtra("daySettings");
                }
            }
        }
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Is an explanation required
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Ask for permission
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        LOCATION_PERMISSION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, request permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_PERMISSION);
            }
            return false;
        } else {
            return true;
        }
    }

    private boolean checkInternetPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

            // Is explanation needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {

                // Ask for permission
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_internet_permission)
                        .setMessage(R.string.text_internet_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.INTERNET},
                                        INTERNET_PERMISSION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.INTERNET},
                        INTERNET_PERMISSION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {

            if (getLastKnownLocation() != null && provider != null) {
                locationManager.requestLocationUpdates(provider, 400, 1, this);

                requestWeatherData();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            locationManager.removeUpdates(this);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        if (provider != null) {
                            locationManager.requestLocationUpdates(provider, 400, 1, this);
                        }
                    }

                }
                return;
            }
            case INTERNET_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.INTERNET)
                            == PackageManager.PERMISSION_GRANTED) {
                        // If internet granted retry request ?
                    }

                }
                return;
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        requestWeatherData();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
