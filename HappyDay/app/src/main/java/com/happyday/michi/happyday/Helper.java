package com.happyday.michi.happyday;

public class Helper {

    public static Rain rainFromViewId(int id) {
        switch (id) {
            case R.id.radiobutton_settings_rain_none:
                return Rain.NONE;
            case R.id.radiobutton_settings_rain_low:
                return Rain.LOW;
            case R.id.radiobutton_settings_rain_medium:
                return Rain.MEDIUM;
            case R.id.radiobutton_settings_rain_high:
                return Rain.HIGH;
            default:
                return Rain.NONE;

        }

    }

    public static int viewIdFromRain(Rain id) {
        if (id.getId() == Rain.NONE.getId()) {
            return R.id.radiobutton_settings_rain_none;
        } else if (id.getId() == Rain.LOW.getId()) {
            return R.id.radiobutton_settings_rain_low;
        } else if (id.getId() == Rain.MEDIUM.getId()) {
            return R.id.radiobutton_settings_rain_medium;
        } else if (id.getId() == Rain.HIGH.getId()) {
            return R.id.radiobutton_settings_rain_high;
        }else{
            return R.id.radiobutton_settings_rain_none;

        }


    }

    public static Wind windFromViewId(int id) {
        switch (id) {
            case R.id.radiobutton_settings_wind_none:
                return Wind.NONE;
            case R.id.radiobutton_settings_wind_low:
                return Wind.LOW;
            case R.id.radiobutton_settings_wind_medium:
                return Wind.MEDIUM;
            case R.id.radiobutton_settings_wind_high:
                return Wind.HIGH;
            default:
                return Wind.NONE;

        }

    }

    public static int viewIdFromWind(Wind id) {

        if (id.getId() == Wind.NONE.getId()) {
            return R.id.radiobutton_settings_wind_none;
        } else if (id.getId() == Wind.LOW.getId()) {
            return R.id.radiobutton_settings_wind_low;
        } else if (id.getId() == Wind.MEDIUM.getId()) {
            return R.id.radiobutton_settings_wind_medium;
        } else if (id.getId() == Wind.HIGH.getId()) {
            return R.id.radiobutton_settings_wind_high;
        }else{
            return R.id.radiobutton_settings_wind_none;

        }

    }

    public static Float scalePercent(int min, int max, int value) {

        if (min > max ) {
            return null;
        }

        int range = max - min;
        float result = ((range * value) / 100f) + min;

        return result;
    }
}
