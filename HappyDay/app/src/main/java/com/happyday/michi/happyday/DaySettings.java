package com.happyday.michi.happyday;

import android.os.Parcel;
import android.os.Parcelable;

public class DaySettings implements Parcelable {

    private int tempMinIndex;
    private int tempMaxIndex;
    private int humidityMin;
    private int humidityMax;
    private Rain rain;
    private Wind wind;

    public DaySettings(){
        this(0,100,0,100, Rain.NONE, Wind.NONE);
    }

    public DaySettings(int tempMin, int tempMax, int humidityMin, int humidityMax, Rain rain, Wind wind) {
        this.tempMinIndex = tempMin;
        this.tempMaxIndex = tempMax;
        this.humidityMin = humidityMin;
        this.humidityMax = humidityMax;
        this.rain = rain;
        this.wind = wind;
    }

    protected DaySettings(Parcel in) {
        tempMinIndex = in.readInt();
        tempMaxIndex = in.readInt();
        humidityMin = in.readInt();
        humidityMax = in.readInt();
        rain = Rain.getEnum(in.readInt());
        wind = Wind.getEnum(in.readInt());
    }

    public static final Creator<DaySettings> CREATOR = new Creator<DaySettings>() {
        @Override
        public DaySettings createFromParcel(Parcel in) {
            return new DaySettings(in);
        }

        @Override
        public DaySettings[] newArray(int size) {
            return new DaySettings[size];
        }
    };

    public int getTempMinIndex() {
        return tempMinIndex;
    }

    public void setTempMinIndex(int tempMinIndex) {
        this.tempMinIndex = tempMinIndex;
    }

    public int getTempMaxIndex() {
        return tempMaxIndex;
    }

    public void setTempMaxIndex(int tempMaxIndex) {
        this.tempMaxIndex = tempMaxIndex;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public int getHumidityMin() {
        return humidityMin;
    }

    public void setHumidityMin(int humidityMin) {
        this.humidityMin = humidityMin;
    }

    public int getHumidityMax() {
        return humidityMax;
    }

    public void setHumidityMax(int humidityMax) {
        this.humidityMax = humidityMax;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    @Override
    public String toString() {
        return "DaySettings{" +
                "tempMinIndex=" + tempMinIndex +
                ", tempMaxIndex=" + tempMaxIndex +
                ", humidityMin=" + humidityMin +
                ", humidityMax=" + humidityMax +
                ", rain=" + rain +
                ", wind=" + wind +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(tempMinIndex);
        parcel.writeInt(tempMaxIndex);
        parcel.writeInt(humidityMin);
        parcel.writeInt(humidityMax);
        parcel.writeInt(rain.getId());
        parcel.writeInt(wind.getId());
    }
}
