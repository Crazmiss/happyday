package com.happyday.michi.happyday;

public class HappyDayJudge {

    public static boolean isNowHappyDay(Weather currentweather, DaySettings daySettings, int tempRangeMin, int tempRangeMax) {

        boolean tempNice = isTempNice(currentweather.getTemp(), daySettings.getTempMinIndex(), daySettings.getTempMaxIndex(), tempRangeMin, tempRangeMax);
        boolean humidityNice = isHumidityNice(currentweather.getHumidity(), daySettings.getHumidityMin(), daySettings.getHumidityMax());
        boolean rainNice = isRainNice(currentweather.getRain(), daySettings.getRain());
        boolean windNice = isWindNice(currentweather.getWindSpeed(), daySettings.getWind());

        return tempNice && humidityNice && rainNice && windNice;
    }

    private static boolean isTempNice(float currentweatherTemp, int minIndex, int maxIndex, int tempRangeMin, int tempRangeMax) {
        float tempMin = Helper.scalePercent(tempRangeMin, tempRangeMax, minIndex);
        float tempMax = Helper.scalePercent(tempRangeMin, tempRangeMax, maxIndex);

        return tempMin <= currentweatherTemp && currentweatherTemp <= tempMax;
    }

    private static boolean isHumidityNice(float humidity, int humidityMin, int humidityMax) {
        return humidityMin <= humidity && humidity <= humidityMax;
    }

    private static boolean isRainNice(int currRain, Rain rain) {
        return rain.getMin() <= currRain && currRain <= rain.getMax();
    }

    private static boolean isWindNice(float currWind, Wind wind) {

        return wind.getMin() <= currWind && currWind <= wind.getMax();
    }
}
