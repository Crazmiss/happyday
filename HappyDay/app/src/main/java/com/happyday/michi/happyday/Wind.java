package com.happyday.michi.happyday;

public enum Wind {

    NONE(1, 0, 5),
    LOW(2, 5, 10),
    MEDIUM(3, 10, 50),
    HIGH(4, 50, 100);

    private int id;
    private int min;
    private int max;

    Wind(int id, int min, int max) {
        this.id = id;
        this.min = min;
        this.max = max;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public static Wind getEnum(int id) {
        for (Wind wind : Wind.values()) {
            if (id == wind.id) {
                return wind;
            }
        }
        return null;
    }


}
