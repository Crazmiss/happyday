package com.happyday.michi.happyday;

public enum Rain {

    NONE(1, 0, 5),
    LOW(2, 5, 10),
    MEDIUM(3, 10, 50),
    HIGH(4, 50, 100);

    private int id;
    private int min;
    private int max;

    Rain(int id, int min, int max) {
        this.id = id;
        this.min = min;
        this.max = max;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public static Rain getEnum(int id) {
        for (Rain rain : Rain.values()) {
            if (id == rain.id) {
                return rain;
            }
        }
        return null;
    }





}
