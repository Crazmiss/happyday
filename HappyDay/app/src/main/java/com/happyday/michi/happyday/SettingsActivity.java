package com.happyday.michi.happyday;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.lpphan.rangeseekbar.RangeSeekBar;


public class SettingsActivity extends AppCompatActivity {

    DaySettings daySettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initUI();
    }

    public void onRadioButtonClicked(View view) {

        ViewParent viewParent = null;

        if (view instanceof RadioButton) {
            viewParent = view.getParent();
        }
        if (viewParent instanceof RadioGroup) {
            animateRadioButton((RadioGroup) viewParent);
        }

    }

    private void animateRadioButton(RadioGroup radioGroup) {

        radioGroup.getCheckedRadioButtonId();

        int count = radioGroup.getChildCount();

        for (int i = 0; i < count; i++) {
            View child = radioGroup.getChildAt(i);

            if (child instanceof RadioButton) {
                if (child.getId() == radioGroup.getCheckedRadioButtonId()) {
                    child.setAlpha(1.f);
                } else {
                    child.setAlpha(0.1f);
                }


            }
        }
    }

    private void initUI() {

        if (getIntent().hasExtra("daySettings") && daySettings == null) {
            daySettings = getIntent().getParcelableExtra("daySettings");
        } else {
            daySettings = new DaySettings();
        }

        if (daySettings == null) {
            return;
        }

        if (daySettings.getRain() == null) {
            daySettings.setRain(Rain.NONE);
        }

//        Set everything that needs to be set
        RangeSeekBar rangeSeekBarTemperature = findViewById(R.id.seekbar_settings_temperature);
        rangeSeekBarTemperature.setLeftIndex(daySettings.getTempMinIndex());
        rangeSeekBarTemperature.setRightIndex(daySettings.getTempMaxIndex());
        rangeSeekBarTemperature.setOnRangeBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangerListener() {
            @Override
            public void onIndexChange(RangeSeekBar rangeBar, final int leftIndex, final int rightIndex) {
                setTemperatureRangeLabels(leftIndex, rightIndex);
            }
        });
        setTemperatureRangeLabels(daySettings.getTempMinIndex(), daySettings.getTempMaxIndex());

        RadioGroup radioGroupRain = findViewById(R.id.radiogroup_settings_rain);
        radioGroupRain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radiobutton_settings_rain_none:
                        blinkAnimation(0, R.id.imageview_settings_drops);
                        break;
                    case R.id.radiobutton_settings_rain_low:
                        blinkAnimation(1000, R.id.imageview_settings_drops);
                        break;
                    case R.id.radiobutton_settings_rain_medium:
                        blinkAnimation(700, R.id.imageview_settings_drops);
                        break;
                    case R.id.radiobutton_settings_rain_high:
                        blinkAnimation(400, R.id.imageview_settings_drops);
                        break;

                }
            }
        });
        // this needs to be done after the listener has started so the animation can be started correctly
        radioGroupRain.check(Helper.viewIdFromRain(daySettings.getRain()));

        if (daySettings.getRain() != null) {
            animateRadioButton((RadioGroup) findViewById(R.id.radiogroup_settings_rain));
        } else {
            RadioButton radioButton = findViewById(R.id.radiobutton_settings_rain_none);
            radioButton.setChecked(true);
        }

        RadioGroup radioGroupWind = findViewById(R.id.radiogroup_settings_wind);
        radioGroupWind.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (i) {
                    case R.id.radiobutton_settings_wind_none:
                        blinkAnimation(0, R.id.imageview_settings_wind);
                        break;
                    case R.id.radiobutton_settings_wind_low:
                        blinkAnimation(1000, R.id.imageview_settings_wind);
                        break;
                    case R.id.radiobutton_settings_wind_medium:
                        blinkAnimation(700, R.id.imageview_settings_wind);
                        break;
                    case R.id.radiobutton_settings_wind_high:
                        blinkAnimation(400, R.id.imageview_settings_wind);
                        break;

                }
            }
        });
        // this needs to be done after the listener has started so the animation can be started correctly
        radioGroupWind.check(Helper.viewIdFromWind(daySettings.getWind()));

        if (daySettings.getWind() != null) {
            animateRadioButton((RadioGroup) findViewById(R.id.radiogroup_settings_wind));
        } else {
            RadioButton radioButton = findViewById(R.id.radiobutton_settings_wind_none);
            radioButton.setChecked(true);
        }

        RangeSeekBar rangeSeekBarHumidity = findViewById(R.id.seekbar_settings_humidity);
        rangeSeekBarHumidity.setLeftIndex(daySettings.getHumidityMin());
        rangeSeekBarHumidity.setRightIndex(daySettings.getHumidityMax());
        setHumidityRangeLabels(daySettings.getHumidityMin(),daySettings.getHumidityMax());

        rangeSeekBarHumidity.setOnRangeBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangerListener() {
            @Override
            public void onIndexChange(RangeSeekBar rangeBar, final int leftIndex, final int rightIndex) {
                setHumidityRangeLabels(leftIndex,rightIndex);

            }
        });

    }

    private void setHumidityRangeLabels(int minIndex, int maxIndex){
        TextView textViewRight = findViewById(R.id.textview_settings_humidity_right);
        textViewRight.setText(String.valueOf(maxIndex));

        TextView textViewLeft = findViewById(R.id.textview_settings_humidity_left);
        textViewLeft.setText(String.valueOf(minIndex));
    }

    private void setTemperatureRangeLabels(int minIndex, int maxIndex) {
        TextView textViewRight = findViewById(R.id.textview_settings_temperature_right);
        TextView textViewLeft = findViewById(R.id.textview_settings_temperature_left);

        int celsiusMin = getResources().getInteger(R.integer.celsiusMin);
        int celsiusMax = getResources().getInteger(R.integer.celsiusMax);

        String leftString = String.format("%.1f", Helper.scalePercent(celsiusMin, celsiusMax, minIndex));
        String rightString = String.format("%.1f", Helper.scalePercent(celsiusMin, celsiusMax, maxIndex));

        textViewRight.setText(rightString);
        textViewLeft.setText(leftString);

    }

    private void blinkAnimation(long duration, int id) {

        if (!(findViewById(id) instanceof ImageView)) {
            return;
        }

        ImageView imageView = findViewById(id);
        Animation animation = imageView.getAnimation();

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.blink);
        }

        if (duration != 0) {
            imageView.setVisibility(View.VISIBLE);
            animation.setDuration(duration);
            imageView.startAnimation(animation);
        } else {
            imageView.setVisibility(View.INVISIBLE);
            imageView.clearAnimation();
        }
    }

    public void BackToMain(View view) {

        RadioGroup radioGroupRain = findViewById(R.id.radiogroup_settings_rain);
        daySettings.setRain(Helper.rainFromViewId(radioGroupRain.getCheckedRadioButtonId()));

        RadioGroup radioGroupWind = findViewById(R.id.radiogroup_settings_wind);
        daySettings.setWind(Helper.windFromViewId(radioGroupWind.getCheckedRadioButtonId()));


        RangeSeekBar rangeSeekBarTemp = findViewById(R.id.seekbar_settings_temperature);
        int tempMaxValue = rangeSeekBarTemp.getRightIndex();
        int tempMinValue = rangeSeekBarTemp.getLeftIndex();

        daySettings.setTempMaxIndex(tempMaxValue);
        daySettings.setTempMinIndex(tempMinValue);


        RangeSeekBar rangeSeekBarHumidity = findViewById(R.id.seekbar_settings_humidity);
        int humidityMaxValue = rangeSeekBarHumidity.getRightIndex();
        int humidityMinValue = rangeSeekBarHumidity.getLeftIndex();

        daySettings.setHumidityMax(humidityMaxValue);
        daySettings.setHumidityMin(humidityMinValue);


        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("daySettings", daySettings);

        setResult(RESULT_OK, intent);
        finish();

    }

}
