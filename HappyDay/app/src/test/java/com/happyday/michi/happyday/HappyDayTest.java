package com.happyday.michi.happyday;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HappyDayTest {

    Weather currentWeather = new Weather();
    DaySettings daySettings = new DaySettings();

    @Test
    public void happyday_isCorrect() {
        currentWeather.setTemp(20.f);
        currentWeather.setHumidity(20);
        currentWeather.setWindSpeed(2.f);
        currentWeather.setRain(10);

        daySettings.setTempMinIndex(0);
        daySettings.setTempMaxIndex(10);
        daySettings.setHumidityMax(100);
        daySettings.setHumidityMin(90);
        daySettings.setWind(Wind.HIGH);
        daySettings.setRain(Rain.NONE);


        assertEquals( false, HappyDayJudge.isNowHappyDay(currentWeather, daySettings, -20, 40 ));
    }
}
