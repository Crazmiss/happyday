package com.happyday.michi.happyday;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ScaleCalcTest {

    @Test
    public void scale_isCorrect() {
        assertEquals( 50.f, Helper.scalePercent(0,100, 50), 0);
    }

    @Test
    public void scale_isInCorrect() {
       assertFalse(2.f == Helper.scalePercent(0,100, 50));
    }

    @Test
    public void scale_isValidationMinBiggerMax() {
        assertTrue(null == Helper.scalePercent(200,100, 50));
    }

}
